/**
symbus Copyright (C) 2010 Neophile
**/

// ****************************************************************************
// *                                                                          *
// *                CMAINFRAME.HPP : Classe Fenetre Principale                *
// *                                                                          *
// *                      Neophile (c) 2010 : 05/12/2010                      *
// *                                                                          *
// ****************************************************************************

#include "cmainframe.hpp"
#include "comdefs.hpp"
#include "logo32x32.xpm"
#include "current.xpm"

wxBEGIN_EVENT_TABLE(CMainFrame, wxFrame)
	EVT_MENU		(MF_MENU_EXIT,		CMainFrame::OnExit)
	EVT_MENU		(MF_MENU_ABOUT,		CMainFrame::OnAbout)
	EVT_TIMER		(MF_UPDATE_STATUS,	CMainFrame::OnUpdateStatus)
	EVT_CLOSE		(					CMainFrame::OnCloseWindow)
wxEND_EVENT_TABLE()

CMainFrame::CMainFrame
	(
	) : wxFrame
	(
		(wxWindow*)NULL,
		wxID_ANY,
		_T(APP_NAME),
		wxDefaultPosition,
		wxDefaultSize,
		wxDEFAULT_FRAME_STYLE
	)
{
	pConfig = wxConfigBase::Get();
	//Positionnement et Dimensionnement de la fenetre
	wxConfigBase::Set(pConfig);
	int	x = pConfig->Read(_T("/Fenetre/prinx"), 50),
		y = pConfig->Read(_T("/Fenetre/priny"), 50),
		w = pConfig->Read(_T("/Fenetre/prinw"), 665),
		h = pConfig->Read(_T("/Fenetre/prinh"), 660);
	if ((x<1) || (y<1))
	{
		x = 50;
		y = 50;
	}

	SetBackgroundColour(_T("#D4D0C8"));
	SetMinSize(wxSize(540,400));

	SetIcon (wxIcon(logo32x32_xpm));

	//Construction de la fenetration;
	MainSplit = new wxSplitterWindow(this, -1, wxDefaultPosition, wxDefaultSize, wxSP_NOBORDER );
	LogZone = new wxTextCtrl(MainSplit, -1, _T(""), wxDefaultPosition, wxSize(-1,-1), wxTE_MULTILINE | wxTE_READONLY);
	Logger = new wxLogTextCtrl (LogZone);
	wxLog::SetActiveTarget(Logger);
	ModBus = new CModBus ();
	Grid = new CByteGrid (MainSplit, ModBus);
	MainSplit->SplitHorizontally(Grid,LogZone);
	MainSplit->SetSashPosition(400);

//Creation du menu
	wxMenu* MenuFichier = new wxMenu;
    MenuFichier->Append(MF_MENU_EXIT, _("&Exit"),_("Exit SymBus"));
	MenuAide = new wxMenu;
	MenuAide->Append(MF_MENU_ABOUT, _("About..."),_("About SymBus"));
	wxMenuBar* Menu = new wxMenuBar;
	Menu->Append(MenuFichier, _("&File"));
	Menu->Append(MenuAide, _("&Help"));
	SetMenuBar(Menu);
	ToolBar = CreateToolBar(wxTB_HORIZONTAL/*|wxTB_HORZ_LAYOUT*/);
	ToolBar->SetToolBitmapSize (wxSize (24,24));
//	ToolBar->AddTool ( CMainFrame::MF_TEST, _("Test"), wxBitmap(current_xpm), _("Test Icons"));
//	ToolBar->Realize ();

    CreateStatusBar(8);
	SetSize(wxSize(w,h));
	SetPosition (wxPoint(x,y));
	//On demarre le timer de mise a jour de la barre des statuts
	TUpdate = new wxTimer(this, MF_UPDATE_STATUS);
	TUpdate->Start(200, false);
}

//------------------------------------------------------------------------------
// Destructeur

CMainFrame::~CMainFrame ()
{
}

//------------------------------------------------------------------------------
// On Quitte le programme

void CMainFrame::OnExit(wxCommandEvent& WXUNUSED(event))
{
	Close();
}

//------------------------------------------------------------------------------
// Fermeture de la fenetre

void CMainFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
	TUpdate->Stop();
	int x, y, w, h;
	if (IsMaximized())
	{
		Maximize(false);
	}
	if (IsIconized())
	{
		Iconize(false);
	}
	GetSize(&w, &h);
	GetPosition(&x, &y);
	pConfig->Write(_T("/Fenetre/prinx"), x);
	pConfig->Write(_T("/Fenetre/priny"), y);
	pConfig->Write(_T("/Fenetre/prinw"), w);
	pConfig->Write(_T("/Fenetre/prinh"), h);
	pConfig->Flush();
	Destroy();
}

//------------------------------------------------------------------------------

void CMainFrame::OnAbout (wxCommandEvent& WXUNUSED(event))
{
    wxAboutDialogInfo info;
    wxString Revision;
    info.SetName(_T(APP_NAME));
    Revision << APP_BUILD
		<< _T(" ")
		<< APP_MAJOR_VER
		<< _T(".")
		<< APP_MINOR_VER
		<< _T(" ");
	info.SetVersion(Revision);
    info.SetDescription(_("FAT Modbus tools."));
    info.SetCopyright(_("(c)2010 Symerion"));
    wxAboutBox(info);
}

//------------------------------------------------------------------------------
// Mise à jour de la barre des statuts

void CMainFrame::OnUpdateStatus (wxTimerEvent& WXUNUSED(event))
{
	wxString Message;
	wxString Titre,Monde;
	// Met à jour la barre de titre
	Titre = _T(APP_NAME);
}
