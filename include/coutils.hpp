/*****************************************************************************
 * Symbus (c) 2010 Neophile                                                  *
 *****************************************************************************/

#ifndef COUTILS_HPP
#define COUTILS_HPP

 #if defined(_WIN32)
 #include <winsock2.h>
 #endif

#ifndef  WX_PRECOMP
	#include <wx/wx.h>
#endif

#include <wx/wxprec.h>

class COutils
{
    public:
		wxString		BinToHex	(unsigned char* dat_ptr=0, size_t Longueur=0);
		size_t			HexToBin	(wxString& Data_Str, unsigned char* dat_ptr=0);
		unsigned short	HexToDec	(const char* Hexa);
		wxString		conv(const std::string &s)
						{
							return wxString(s.c_str(), wxConvUTF8);
						}

		std::string		conv(const wxString &s)
						{
							return std::string(s.mb_str(wxConvUTF8));
						}
		static wxString		configDir;  ///< Full name of the directory which contains the application's preferences.
		static wxString		configFileName;  ///< Name of the file which contains the application preferences.

		static wxString		getConfigDir();
		static wxString		getConfigFileName();
		static wxString		getConfigPathName();
		static wxString		getConfigDirName();
};

#endif
