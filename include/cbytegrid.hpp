#ifndef CBYTEGRID_HPP
#define CBYTEGRID_HPP

#if defined(_WIN32)
#include <winsock2.h>
#endif

#ifndef  WX_PRECOMP
	#include <wx/wx.h>
#endif

#include <wx/wxprec.h>
#include <wx/grid.h>


#include "cmodbus.hpp"

class CByteGrid : public wxGrid
{
	public:
					CByteGrid (wxWindow* pParent,CModBus* pModbus);
					~CByteGrid ();
	private:
		CModBus*	ModBus;
		wxTimer*	UpdateTimer;

	protected:
		enum
		{
			CBG_UPDATE = wxID_HIGHEST + 1
		};
		void		OnUpdate (wxTimerEvent& event);
		void		OnDblClick (wxGridEvent& event);
		wxDECLARE_EVENT_TABLE();
};

#endif
