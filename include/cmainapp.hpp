/**
Symbus Copyright (C) 2010 Neophile
**/
// ****************************************************************************
// *                                                                          *
// *                 MAINAPP.H : Classe d'application principale              *
// *                                                                          *
// *                      Neophile (c) 2010 : 05/12/2010                      *
// *                                                                          *
// ****************************************************************************

#ifndef CMAINAPP_HPP
#define CMAINAPP_HPP

#if defined(_WIN32)
#include <winsock2.h>
#endif

#ifndef  WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/wxprec.h>

#include <wx/intl.h>
#include <wx/fileconf.h>

#define NB_MAX_LNG 3

#ifdef __WXMAC__
	#include <ApplicationServices/ApplicationServices.h>
#endif

// language data
static const wxLanguage langIds[] =
{
	wxLANGUAGE_ENGLISH,
	wxLANGUAGE_FRENCH,
	wxLANGUAGE_SPANISH
};

// note that it makes no sense to translate these strings, they are
// shown before we set the locale anyhow
const wxString langNames[] =
{
	_T("English"),
	_T("French"),
	_T("Spanish")
};

wxCOMPILE_TIME_ASSERT(WXSIZEOF(langNames) == WXSIZEOF(langIds),
	LangArraysMismatch);

// Classe d'application g�n�rale

class CMainApp : public wxApp
{
	private:
		wxFileConfig*		pConfig;
#ifdef __WXMAC__
		ProcessSerialNumber		PSN;
#endif
	protected:
		virtual bool	OnInit();
		virtual int 	OnExit();
		wxLocale		m_locale;
};

#endif

