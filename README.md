SymBus is light tcp modbus slave server tool for commissioning, testing Industrial PLC

It's writting under C++, wxWidgets FrameWork and LibModbus Library 

Reference:
https://www.wxwidgets.org/
http://libmodbus.org/

This tool as been optimized to be crossplatform, and most easy to use

In future, it could work also in RTU (RS232 or RS485/422 with industrial cards) and can also act as master with query wizzard.
All volonteer are welcome to this project. If you are interested, please take contact through github on my profil.
