/**
Symbus Copyright (C) 2010 Neophile
**/

// ****************************************************************************
// *                                                                          *
// *                  MAINFRAME.H : Classe Fenetre Principale                 *
// *                                                                          *
// *                      Neophile (c) 2010 : 05/12/2010                      *
// *                                                                          *
// ****************************************************************************

#ifndef CMAINFRAME_H
#define CMAINFRAME_H

#if defined(_WIN32)
#include <winsock2.h>
#endif

#ifndef  WX_PRECOMP
	#include <wx/wx.h>
#endif

#include <wx/wxprec.h>

#include "wx/config.h"
#include <wx/aboutdlg.h>
#include <wx/toolbar.h>
#include <wx/log.h>
#include <wx/splitter.h>

#include "coutils.hpp"
#include "cmodbus.hpp"
#include "cbytegrid.hpp"

enum MF_COMMANDS
{
	MF_MENU_EXIT = wxID_HIGHEST,
	MF_MENU_ABOUT,
	MF_UPDATE_STATUS
};

class CMainFrame : public wxFrame, public COutils
{
	public:
							CMainFrame ();
		virtual				~CMainFrame ();

	private:
		wxConfigBase* 			pConfig;
		// Declaration des widgets
		wxTimer*			TUpdate;
        wxToolBar*          ToolBar;
        wxLogTextCtrl*		Logger;
        wxTextCtrl*			LogZone;
        wxSplitterWindow*	MainSplit;
        wxBoxSizer*			UpSizer;

		CModBus*			ModBus;
		CByteGrid*			Grid;

		// Menus
		wxMenuBar*			MenuBar;
		wxMenu*				MenuFichier;
		wxMenu*				MenuAide;

	protected:

		void				OnExit (wxCommandEvent& event);
		void				OnAbout (wxCommandEvent& event);
		void				OnUpdateStatus (wxTimerEvent& event);
		void				OnCloseWindow(wxCloseEvent& event);
		wxDECLARE_EVENT_TABLE();
};

#endif

