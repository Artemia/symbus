#ifndef cmodbus_hpp
#define cmodbus_hpp

#if defined(_WIN32)
#include <winsock2.h>
#endif

#ifndef  WX_PRECOMP
	#include <wx/wx.h>
#endif

#include <wx/wxprec.h>

#include <wx/log.h>

#include <modbus.h>

#define NB_CONNECTION    5
#define MODBUS_MAX_REGISTERS 1999

class CModBus : public wxEvtHandler
{
	public:
							CModBus ();
							~CModBus ();
		void				Close ();
		bool				GetRegister (unsigned int Pos, short& Value);
		bool				SetRegister (unsigned int Pos, short Value);
		bool				GetBit		(unsigned int Pos, unsigned char Bit, bool& Value);
		bool				SetBit		(unsigned int Pos, unsigned char Bit, bool Value);
	private:
		modbus_t*			ctx;
		int					server_socket;
		int					master_socket;
		fd_set				refset;
		fd_set				rdset;
		/* Maximum file descriptor number */
		int fdmax;
		modbus_mapping_t*	mb_mapping;
		wxTimer*			UpdateTimer;
		int					rc;
		bool				Init;
	protected:
		enum
		{
			CMB_UPDATE= wxID_HIGHEST + 1
		};
		void				OnUpdate (wxTimerEvent& event);
		wxDECLARE_EVENT_TABLE();
};

#endif
