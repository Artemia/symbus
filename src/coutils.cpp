/*****************************************************************************
 * Symbus (c) 2010 Neophile                                                  *
 *****************************************************************************/

#include "coutils.hpp"

unsigned short COutils::HexToDec(const char* Hexa)
{
	unsigned short valeur=0,res=0;
	int i=0;
	int Long=strlen (Hexa);
	if ((Long==0)||(Long>4)) return 0;
	for (i=0;i<Long;i++)
	{
		if ((Hexa[i]>='0')&&(Hexa[i]<='9')) valeur=Hexa[i]-48;
		else if ((Hexa[i]>='A')||(Hexa[i]<='F')) valeur=Hexa[i]-55;
		else if ((Hexa[i]>='a')||(Hexa[i]<='f')) valeur=Hexa[i]-87;
		else valeur=0;
		valeur=valeur*(unsigned short)(pow(16,(double)((Long-1)-i)));
		res+=valeur;
	}
	return res;
}

//-----------------------------------------------------------------------------
// Convert a buffer to a string hexadecimal notation

wxString COutils::BinToHex	(unsigned char* dat_ptr, size_t Longueur)
{
	wxString Resultat('0',Longueur*2);
	char Tampon [3];
	for (size_t i=0; i<Longueur; i++)
	{
		sprintf (Tampon,"%02X", dat_ptr[i]);
		Resultat[i*2]=Tampon[0];
		Resultat[(i*2)+1]=Tampon[1];
	}
	return Resultat;
}

//-----------------------------------------------------------------------------
// Convert String in hexadecimal notation to binary buffer and return the length

size_t COutils::HexToBin (wxString& Data_Str, unsigned char* dat_ptr)
{
	char Tampon[3];
	size_t Longueur=Data_Str.Len()/2;
	for (size_t i=0 ; i<Longueur ; i++)
	{
		Tampon[0]=Data_Str[(i*2)];
		Tampon[1]=Data_Str[(i*2)+1];
		Tampon[2]='\0';
		dat_ptr[i]=HexToDec(Tampon);
	}
	return Longueur;
}