/*
 * Symbus (c) 20100 Neophile
*/

#ifndef COMDEFS_HPP
#define COMDEFS_HPP


/// Application's major version number
#define  APP_MAJOR_VER  0

/// Application's minor version number
#define  APP_MINOR_VER  1

/// Application's subversion number
#define  APP_SUBVER     0

/// Application's build number
#define  APP_BUILD      1

/// Application's special (like 'alpha', 'beta', 'rc')
#define  APP_SPECIAL    "alpha"

/// Application's name
#define  APP_NAME       "symbus"

/// Application's author/vendor
#define  APP_AUTHOR     "Gianni Peschiutta"

/// Application's development years
#define  APP_DEV_DATES  "2010"

/// The space between 2 controls.
#define  CONTROL_SPACE  10

#endif
