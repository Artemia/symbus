#include "cbytegrid.hpp"
#include <wx/numdlg.h>

wxBEGIN_EVENT_TABLE(CByteGrid, wxGrid)
	EVT_TIMER					(CByteGrid::CBG_UPDATE, CByteGrid::OnUpdate)
	EVT_GRID_CELL_LEFT_DCLICK	(CByteGrid::OnDblClick)
wxEND_EVENT_TABLE()

CByteGrid::CByteGrid (wxWindow* pParent, CModBus* pModbus)
: wxGrid (pParent,-1)
{
	ModBus = pModbus;
	CreateGrid (MODBUS_MAX_REGISTERS,17);
	SetColLabelValue(0,_("Integer"));
	SetColSize(0,75);
	for (int i=1; i<17; i++)
	{
		wxString Tempo;
		Tempo << i-1;
		SetColLabelValue (17-i,Tempo);
		SetColSize(i,25);
	}
	for (int i=0; i<MODBUS_MAX_REGISTERS; i++)
	{
		wxString Tempo;
		Tempo << i+40001;
		SetRowLabelValue(i,Tempo);
	}
	EnableEditing (false);
	UpdateTimer = new wxTimer (this,CBG_UPDATE);
	UpdateTimer->Start(500,false);
}

CByteGrid::~CByteGrid ()
{

}

void CByteGrid::OnUpdate (wxTimerEvent& WXUNUSED(event))
{
	short Register;
	bool Bit;
	wxString Tempo;
	for (int i = 0; i<MODBUS_MAX_REGISTERS; i++)
	{
		if (IsVisible(i,0,false))
		{
			ModBus->GetRegister(i,Register);
			Tempo=_T("");
			Tempo << Register;
			SetCellValue (i,0,Tempo);
			for (unsigned char j=0; j<16; j++)
			{
				ModBus->GetBit(i,j,Bit);
				Tempo=_T("");
				Tempo << Bit;
				SetCellValue (i,16-j,Tempo);
			}
		}
	}
}

void CByteGrid::OnDblClick(wxGridEvent& event)
{
	int col=event.GetCol();
	int row=event.GetRow();
	if (col==0)
	{
		long Valeur;
		short registre;
		ModBus->GetRegister(row, registre);
		Valeur = ::wxGetNumberFromUser (_("Please enter a value:"), _("Set a register"), _T(""),registre,-32768, 32767);
		ModBus->SetRegister(row, (short)Valeur);
	}
	else
	{
		bool Bit;
		ModBus->GetBit(row,16-col,Bit);
		Bit = !Bit;
		ModBus->SetBit(row,16-col,Bit);
	}
}
