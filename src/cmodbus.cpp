#include "cmodbus.hpp"

#ifndef  _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cerrno>
#else
#include <ws2tcpip.h>
#endif

wxBEGIN_EVENT_TABLE(CModBus, wxEvtHandler)
	EVT_TIMER		(CModBus::CMB_UPDATE, CModBus::OnUpdate)
wxEND_EVENT_TABLE()

CModBus::CModBus ()
{
	Init=false;
	ctx = modbus_new_tcp("127.0.0.1", 1502);
	mb_mapping = modbus_mapping_new(MODBUS_MAX_READ_BITS, MODBUS_MAX_READ_BITS,
                                    MODBUS_MAX_REGISTERS, MODBUS_MAX_REGISTERS);
	if (mb_mapping == NULL)
	{
        wxLogMessage(_("Failed to allocate the mapping: %s\n"),
                modbus_strerror(errno));
        modbus_free(ctx);
        return;
    }
	server_socket = modbus_tcp_listen(ctx, NB_CONNECTION);

	/* Clear the reference set of socket */
	FD_ZERO(&refset);
	/* Add the server socket */
	FD_SET(server_socket, &refset);

	/* Keep track of the max file descriptor */
	fdmax = server_socket;
    UpdateTimer = new wxTimer (this,CMB_UPDATE);
    UpdateTimer->Start(200,false);
    wxLogMessage (_("All is ok, server running"));
    Init=true;
}

void CModBus::Close ()
{
	if (Init)
	{
		wxLogMessage (_("Server stopped"));
		UpdateTimer->Stop();
		close(server_socket);
		modbus_free(ctx);
		modbus_mapping_free(mb_mapping);
		Init=false;
	}
}

CModBus::~CModBus ()
{
	Close ();
}

void CModBus::OnUpdate(wxTimerEvent& WXUNUSED(event))
{
	if (!Init) return;
	struct timeval waitd;
	waitd.tv_usec = 1000;
	waitd.tv_sec = 0;

	rdset = refset;
	rc = select(fdmax + 1, &rdset, NULL, NULL, &waitd);
	if (rc == -1)
	{
		wxLogMessage(_("Server select() failure."));
		Close();
		return;
	}
	if (rc == 0)
	{
		//wxLogMessage (_("Nothing to hear"));
		return;
	}
	/* Run through the existing connections looking for data to be
	* read */
	for (master_socket = 0; master_socket <= fdmax; master_socket++)
	{
		if (FD_ISSET(master_socket, &rdset))
		{
			if (master_socket == server_socket)
			{
				/* A client is asking a new connection */
				socklen_t addrlen;
				struct sockaddr_in clientaddr;
				int newfd;

				/* Handle new connections */
				addrlen = sizeof(clientaddr);
				memset(&clientaddr, 0, sizeof(clientaddr));
				newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
				if (newfd == -1)
				{
					wxLogMessage(_("Server accept() error"));
				}
				else
				{
					FD_SET(newfd, &refset);

					if (newfd > fdmax)
					{
						/* Keep track of the maximum */
						fdmax = newfd;
					}
					wxString Message;
					Message << _("New connection from ")
						<< wxString::FromAscii(inet_ntoa(clientaddr.sin_addr))
						<< _T(":")
						<< clientaddr.sin_port
						<< _(" on socket ")
						<< newfd;
					wxLogMessage(Message);
				}
			}
			else
			{
				/* An already connected master has sent a new query */
				uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
				modbus_set_socket(ctx, master_socket);
				rc = modbus_receive(ctx, query);
				if (rc != -1)
				{
					modbus_reply(ctx, query, rc, mb_mapping);
				}
				else
				{
					/* Connection closed by the client, end of server */
					wxLogMessage(_("Connection closed on socket %d\n"), master_socket);
					close(master_socket);

					/* Remove from reference set */
					FD_CLR(master_socket, &refset);

					if (master_socket == fdmax)
					{
						fdmax--;
					}
				}
			}
		}
	}
}

bool	CModBus::GetRegister (unsigned int Pos, short& Value)
{
	if (Pos >= mb_mapping->nb_registers ) return false;
	Value = (short)(mb_mapping->tab_registers[Pos]);
	return true;
}
bool	CModBus::SetRegister (unsigned int Pos, short Value)
{
	if (Pos >= mb_mapping->nb_registers ) return false;
	mb_mapping->tab_registers[Pos]=Value;
	return true;
}
bool	CModBus::GetBit		(unsigned int Pos, unsigned char Bit, bool& Value)
{
	if (Pos >= mb_mapping->nb_registers ) return false;
	if (Bit >= 16) return false;
	short Registre = mb_mapping->tab_registers[Pos];
	Registre = Registre >> Bit;
	Value = Registre & 0b0000000000000001;
	return true;
}
bool	CModBus::SetBit		(unsigned int Pos, unsigned char Bit, bool Value)
{
	if (Pos >= mb_mapping->nb_registers ) return false;
	if (Bit >= 16) return false;
	short Registre = mb_mapping->tab_registers[Pos];
	short Masque;
	Masque=1;
	Masque=Masque << Bit;
	if (!Value)
	{
		Masque = Masque ^ 0xFFFF;
		Registre= Registre & Masque;
	}
	else
	{
		Registre = Registre | Masque;
	};
	mb_mapping->tab_registers[Pos]=Registre;
}
